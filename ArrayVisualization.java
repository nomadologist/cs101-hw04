/**
 * Generates random array of circle and rectangles and prints them using Processing
 * 
 * @author Jack Booth
 * @version 11/1/16
 */
import processing.core.*;


public class ArrayVisualization extends PApplet
{
	public static void main(String[] args)
	{
		PApplet.main("ArrayVisualization");
	}
	
//	Sets size
	public void settings()
	{
		size(500, 500);
	}
	
	public void setup()
	{
//		initializations
		background(0);
		int[][] grid = new int[5][5];
		
//		creates random grid of integers that are randomly assigned 0 or 1
		for(int i = 0; i < 5; i++)
		{
			for(int j = 0; j<5; j++)
			{
				grid[i][j] = (int)(Math.random()*2);
			}
		}
		
//		Goes through each 100th pixel and assigns either a circle or rectangle based on the corresponding grid value.
		for(int row = 50; row<height; row+=100)
		{
			for(int col = 50; col<width; col+=100)
			{
				if(grid[(row/100)][(col/100)] == 0)
				{
					ellipse(row, col, 50, 50);
				}
				else if(grid[(row/100)][(col/100)] == 1)
				{
					rect((row-25), (col-25), 50, 50);
				}
			}
		}
	}
	
	public void draw()
	{
		
	}
}

