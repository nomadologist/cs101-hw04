/**
 * Generates a falling circle of constantly changing color that falls for 1.5 seconds and rises for the rest of the time.
 * 
 * @author Jack Booth
 * @version 11/1/16
 */
import processing.core.*;

public class GifsAreGreat extends PApplet
{
//	Variable declarations
	int x, y;
	int frameCount = 0;
	
	public static void main(String[] args)
	{
		PApplet.main("GifsAreGreat");
	}
	
//	Sets size and initial x and y position of circle
	public void settings()
	{
		size(500, 500);
		x = width/2;
		y = height/2;
	}
	
//	Sets background color to white
	public void setup()
	{
		background(255);
	}
	
	public void draw()
	{
//		Generates random RGB values
		int[] randRGB = {(int)(Math.random() * 256), (int)(Math.random() * 256), (int)(Math.random() * 256)};
		
		background(255);
		
//		Constantly changes to random color
		fill(randRGB[0], randRGB[1], randRGB[2]);
		rect(x, y, 100, 100);
		frameCount++;
//		Moves down for 1.5 sec
		if(frameCount <=90)
			y++;
//		Moves up after 1.5 sec until program is ended
		else if(frameCount > 90)
			y--;
		
	}
}
